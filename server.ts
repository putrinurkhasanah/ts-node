import mongoose from 'mongoose';
import userRouter from './src/routes/index'; 
import express, { Request, Response } from 'express';

const PORT = 3000;
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', userRouter);

app.get('/', (req: Request, res: Response) => res.json({
  success: true,
  data: '',
  message: 'Halo, apa kabar?',
  code: 200
}));

app.get('*', (req: Request, res: Response) => res.json({
  success: false,
  data: '',
  message: 'Tidak ditemukan',
  code: 404
}));

mongoose.connect('mongodb://localhost:27017/test-users', { 
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true
}, (err) => err ? console.warn('Error di database') : console.log('Database sukses'));

app.listen(PORT, () => console.log(`Terkoneksi di ${PORT}`))
