import * as bcrypt from 'bcrypt'; 
import { Request, Response } from 'express';

import { User } from '../../models/userModels';

export default class userController { 
  public static getUsers = async (req: Request, res: Response) => {
    const user = await User.find({}); 

    return res.status(200).json({ 
      success: true,
      data: user,
      message: 'Daftar user berhasil ditampilkan',
      code: 200
    });
  };

  public static createUsers = async (req: Request, res: Response) => {
    const data = req.body; 
    const salting = await bcrypt.genSalt(10);
    data.password = await bcrypt.hash(data.password, salting); 

    const user = new User({ ...data });
    await user.save(); 
    return res.status(200).json({ 
      success: true,
      data: user,
      message: 'User berhasil dibuat',
      code: 200
    });
  }

  public static loginUser = async (req: Request, res: Response) => {
    const data = req.body;
    const user = await User.findOne({ email: data.email }); 
    if (user) {
      const validatePassword = await bcrypt.compare(data.password, user.password); 
      if (validatePassword) { 
        return res.status(200).json({
          success: true,
          data: user,
          message: 'Anda telah login!',
          code: 200
        });
      } 
      return res.status(401).json({ 
        success: false,
        data: null,
        message: 'Password anda salah!',
        code: 401
      });
    }

    return res.status(404).json({ 
      success: false,
      data: null,
      message: 'User tidak ditemukan!',
      code: 404
    });
  }
}
