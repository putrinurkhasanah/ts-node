import express from 'express';
const router = express.Router();

import userController from '../controllers/userController'; 

router.get('/api/user', userController.getUsers); 
router.post('/api/user/login', userController.loginUser);
router.post('/api/user/register', userController.createUsers);

export default router;
